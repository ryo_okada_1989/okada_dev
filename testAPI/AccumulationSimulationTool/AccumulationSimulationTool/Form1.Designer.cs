﻿namespace AccumulationSimulationTool
{
    partial class Form1
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.DDBoxFoleder = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.priceBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.start_year = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.start_month = new System.Windows.Forms.ComboBox();
            this.start_day = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // DDBoxFoleder
            // 
            this.DDBoxFoleder.AllowDrop = true;
            this.DDBoxFoleder.Location = new System.Drawing.Point(12, 33);
            this.DDBoxFoleder.Multiline = true;
            this.DDBoxFoleder.Name = "DDBoxFoleder";
            this.DDBoxFoleder.Size = new System.Drawing.Size(445, 79);
            this.DDBoxFoleder.TabIndex = 0;
            this.DDBoxFoleder.DragDrop += new System.Windows.Forms.DragEventHandler(this.DDBoxFoleder_DragDrop);
            this.DDBoxFoleder.DragEnter += new System.Windows.Forms.DragEventHandler(this.DDBoxFoleder_DragEnter);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "読み込みフォルダ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(68, 208);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "購入額";
            // 
            // priceBox
            // 
            this.priceBox.Location = new System.Drawing.Point(139, 205);
            this.priceBox.Name = "priceBox";
            this.priceBox.Size = new System.Drawing.Size(90, 19);
            this.priceBox.TabIndex = 2;
            this.priceBox.Text = "5000";
            this.priceBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.priceBox_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 241);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 12);
            this.label3.TabIndex = 3;
            this.label3.Text = "積み立て開始時期";
            // 
            // start_year
            // 
            this.start_year.Location = new System.Drawing.Point(139, 238);
            this.start_year.Name = "start_year";
            this.start_year.Size = new System.Drawing.Size(38, 19);
            this.start_year.TabIndex = 4;
            this.start_year.Text = "2017";
            this.start_year.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.start_year_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(235, 208);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(17, 12);
            this.label4.TabIndex = 5;
            this.label4.Text = "円";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(183, 241);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(17, 12);
            this.label5.TabIndex = 5;
            this.label5.Text = "年";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(258, 241);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(17, 12);
            this.label6.TabIndex = 5;
            this.label6.Text = "月";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(340, 241);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(17, 12);
            this.label7.TabIndex = 5;
            this.label7.Text = "日";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 290);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(445, 50);
            this.button1.TabIndex = 6;
            this.button1.Text = "結果出力";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // start_month
            // 
            this.start_month.FormattingEnabled = true;
            this.start_month.Items.AddRange(new object[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12"});
            this.start_month.Location = new System.Drawing.Point(200, 238);
            this.start_month.Name = "start_month";
            this.start_month.Size = new System.Drawing.Size(52, 20);
            this.start_month.TabIndex = 8;
            this.start_month.Text = "01";
            // 
            // start_day
            // 
            this.start_day.FormattingEnabled = true;
            this.start_day.Items.AddRange(new object[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31"});
            this.start_day.Location = new System.Drawing.Point(281, 238);
            this.start_day.Name = "start_day";
            this.start_day.Size = new System.Drawing.Size(52, 20);
            this.start_day.TabIndex = 9;
            this.start_day.Text = "01";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 124);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(451, 12);
            this.label8.TabIndex = 10;
            this.label8.Text = "Daily,Monthly,Weeklyのエクセルファイルが格納されたフォルダをドラッグアンドドロップしてください。";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(469, 348);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.start_day);
            this.Controls.Add(this.start_month);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.start_year);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.priceBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DDBoxFoleder);
            this.Name = "Form1";
            this.Text = "AccumulationSimulationTool";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox DDBoxFoleder;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox priceBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox start_year;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox start_month;
        private System.Windows.Forms.ComboBox start_day;
        private System.Windows.Forms.Label label8;
    }
}

