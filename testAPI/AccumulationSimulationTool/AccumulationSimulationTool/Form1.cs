﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace AccumulationSimulationTool
{
    public partial class Form1 : Form
    {
        /// <summary>
        /// ファイルパス
        /// </summary>
        string allFilePath;

        public Form1()
        {
            InitializeComponent();
        }

        #region 読み込み処理
        private void DDBoxFoleder_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop, false);

            for (int i = 0; i < files.Length; i++)
            {
                if (i != 0)
                {
                    allFilePath += "@";
                }
                string[] deepfiles = Directory.GetFiles(files[i], "*.csv", System.IO.SearchOption.AllDirectories);

                for (int j = 0; j < deepfiles.Length; j++)
                {
                    string fileName = deepfiles[j];
                    DDBoxFoleder.Text += fileName + "\r\n";
                    allFilePath += fileName + "@";
                }
            }
        }

        private void DDBoxFoleder_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.All;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }
        #endregion

        private void button1_Click(object sender, EventArgs e)
        {
            //開始年月日
            string year = start_year.Text;
            string month = start_month.Text;
            string day = start_day.Text;
            string ymd = year + "/" + month + "/" + day;

            bool ymd_flg = false;   //計算開始位置フラグ
            bool calc_flg = false;  //計算開始フラグ


            decimal buy_btc = 0;        //購入BTC
            decimal total_buy_btc = 0;  //合計BTC
            int total_money = 0;        //積立金
            int profit = 0;             //利益

            #region データ抽出

            allFilePath = allFilePath.Replace("@@", "@");
            string[] csvFile = allFilePath.Split('@');//読み込みファイル

            // 文字コードを指定
            Encoding enc = Encoding.GetEncoding("Shift_JIS");

            // ファイルを開く
            StreamWriter writer = new StreamWriter(System.IO.Directory.GetCurrentDirectory() + "\\simulation_result.csv", false, enc);

            //ヘッダー設定
            writer.Write("購入日,始値,購入BTC量,購入BTC合計,合計積み立て額,売却時の利益\r\n");

            for (int i = 0; i < csvFile.Length; i++)//読み込みファイル分ループ
            {
                //初期化
                ymd_flg = false;
                total_money = 0;
                total_buy_btc = 0;

                if (csvFile[i].Contains("Daily")) writer.WriteLine("daily");
                else if (csvFile[i].Contains("Weekly")) writer.WriteLine("weekly");
                else if (csvFile[i].Contains("Monthly")) writer.WriteLine("monthly");
                else if (csvFile[i]=="") continue;

                Dictionary<string, int> BTC_Value = new Dictionary<string, int>();  //BTC価格辞書

                StreamReader read = new StreamReader(csvFile[i], Encoding.GetEncoding("Shift_JIS"));

                string allRead = read.ReadToEnd().Replace("\n", "@");   //ファイルデータすべて読み込み
                string[] readBunkatu = allRead.Split('@');              //１行ごとに分割
                string kakikomi = "";                                   //書き込み内容

                //詳細データ読み込み
                for(int j = 1; j <= readBunkatu.Length -1;j++)
                {
                    string[] t_ymd;     //年月日変換用
                    string henkan_ymd;  //変換後年月日

                    if (readBunkatu[j] == "") continue;

                    readBunkatu[j] = readBunkatu[j].Replace("\t", "@");
                    string[] t_readBunkatu = readBunkatu[j].Split('@');

                    t_ymd = t_readBunkatu[0].Split('.');
                    henkan_ymd = t_ymd[0] + "/" + t_ymd[1] + "/" + t_ymd[2];

                    BTC_Value.Add(henkan_ymd, int.Parse(t_readBunkatu[1]));
                }

                //開始位置確認
                while(ymd_flg)
                {
                    if(!BTC_Value.ContainsKey(ymd)) //開始日付が無ければ日程ずらす
                    {
                        day = (int.Parse(day) + 1).ToString();  //１日ずらす

                        if (int.Parse(day) >= 31)//31日を超えたら
                        {
                            month = (int.Parse(month) + 1).ToString();
                        }

                        if(int.Parse(month) >= 12)//12月を超えたら
                        {
                            year = (int.Parse(year) + 1).ToString();
                        }
                        
                        ymd = year + "/" + month + "/" + day;
                    }
                    else
                    {
                        ymd_flg = true;
                    }
                }

                //計算
                foreach(KeyValuePair<string, int> item in BTC_Value)
                {
                    //初期化
                    kakikomi = "";

                    //指定日付より前ならスキップ
                    if (calc_flg || ymd.CompareTo(item.Key) == 0) calc_flg = true;
                    else continue;

                    if (calc_flg)
                    {
                        kakikomi += item.Key + ",";     //購入日
                        kakikomi += item.Value + ",";   //始値

                        //購入BTC
                        buy_btc = decimal.Parse(priceBox.Text) / (decimal)item.Value;
                        kakikomi += buy_btc.ToString("F4") + ",";

                        //合計BTC
                        total_buy_btc += buy_btc;
                        kakikomi += total_buy_btc.ToString("F4") + ",";

                        //積立金額
                        total_money += int.Parse(priceBox.Text);
                        kakikomi += total_money + ",";

                        //売却益
                        profit = decimal.ToInt32(((decimal)item.Value * total_buy_btc) - (decimal)total_money);
                        kakikomi += profit;
                    }

                    // 結果出力
                    writer.WriteLine(kakikomi);
                }
                read.Close();
            }
            // ファイルを閉じる
            writer.Close();
            #endregion

            MessageBox.Show("出力完了");

        }

        private void start_year_KeyPress(object sender, KeyPressEventArgs e)
        {
            //KeyControl(e);
        }

        private void priceBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            //KeyControl(e);
        }

        //入力制限
        void KeyControl(KeyPressEventArgs e)
        {
            if (e.KeyChar < '0' || '9' < e.KeyChar)
            {
                //押されたキーが 0～9でない場合は、イベントをキャンセルする
                e.Handled = true;
            }
        }
    }
}
