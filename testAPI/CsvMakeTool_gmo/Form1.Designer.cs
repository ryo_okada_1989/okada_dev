﻿namespace CsvMakeTool
{
    partial class Form1
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.DDBOX = new System.Windows.Forms.TextBox();
            this.csvMakeBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.DDBOXFOLDER = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // DDBOX
            // 
            this.DDBOX.AllowDrop = true;
            this.DDBOX.Location = new System.Drawing.Point(638, 354);
            this.DDBOX.Multiline = true;
            this.DDBOX.Name = "DDBOX";
            this.DDBOX.Size = new System.Drawing.Size(69, 43);
            this.DDBOX.TabIndex = 0;
            this.DDBOX.DragDrop += new System.Windows.Forms.DragEventHandler(this.DDBOX_DragDrop);
            this.DDBOX.DragEnter += new System.Windows.Forms.DragEventHandler(this.DDBOX_DragEnter);
            // 
            // csvMakeBtn
            // 
            this.csvMakeBtn.Location = new System.Drawing.Point(106, 293);
            this.csvMakeBtn.Name = "csvMakeBtn";
            this.csvMakeBtn.Size = new System.Drawing.Size(206, 90);
            this.csvMakeBtn.TabIndex = 1;
            this.csvMakeBtn.Text = "CSV作成";
            this.csvMakeBtn.UseVisualStyleBackColor = true;
            this.csvMakeBtn.Click += new System.EventHandler(this.csvMakeBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(540, 371);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "読み込みファイル";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "読み込みフォルダ";
            // 
            // DDBOXFOLDER
            // 
            this.DDBOXFOLDER.AllowDrop = true;
            this.DDBOXFOLDER.Location = new System.Drawing.Point(106, 12);
            this.DDBOXFOLDER.Multiline = true;
            this.DDBOXFOLDER.Name = "DDBOXFOLDER";
            this.DDBOXFOLDER.Size = new System.Drawing.Size(634, 275);
            this.DDBOXFOLDER.TabIndex = 0;
            this.DDBOXFOLDER.TextChanged += new System.EventHandler(this.DDBOXFOLDER_TextChanged);
            this.DDBOXFOLDER.DragDrop += new System.Windows.Forms.DragEventHandler(this.DDBOXFOLDER_DragDrop);
            this.DDBOXFOLDER.DragEnter += new System.Windows.Forms.DragEventHandler(this.DDBOXFOLDER_DragEnter);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(318, 293);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(206, 90);
            this.button1.TabIndex = 1;
            this.button1.Text = "クリア";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.csvMakeBtn);
            this.Controls.Add(this.DDBOXFOLDER);
            this.Controls.Add(this.DDBOX);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox DDBOX;
        private System.Windows.Forms.Button csvMakeBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox DDBOXFOLDER;
        private System.Windows.Forms.Button button1;
    }
}

