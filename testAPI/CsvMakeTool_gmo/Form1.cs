﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace CsvMakeTool
{
    public partial class Form1 : Form
    {
        string allFilePath;

        public Form1()
        {
            InitializeComponent();
        }


        #region■読み込み
        //ファイル読み込み処理
        private void DDBOX_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop, false);
            for (int i = 0; i < files.Length; i++)
            {
                if (i != 0)
                {
                    allFilePath += "@";
                }
                string fileName = files[i];
                DDBOX.Text += fileName + "\r\n";
                allFilePath += fileName;
            }
        }

        private void DDBOX_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.All;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        //フォルダ読み込み
        private void DDBOXFOLDER_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop, false);

            for (int i = 0; i < files.Length; i++)
            {
                if (i != 0)
                {
                    allFilePath += "@";
                }
                string[] deepfiles = Directory.GetFiles(files[i], "*.csv", System.IO.SearchOption.AllDirectories);

                for (int j = 0; j < deepfiles.Length; j++)
                {
                    string fileName = deepfiles[j];
                    DDBOXFOLDER.Text += fileName + "\r\n";
                    allFilePath += fileName + "@";
                }
            }
        }

        private void DDBOXFOLDER_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.All;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        #endregion
        private void csvMakeBtn_Click(object sender, EventArgs e)
        {
            allFilePath = allFilePath.Replace("@@", "@");
            string[] csvFile = allFilePath.Split('@');//読み込みファイル
            Array.Sort(csvFile);//昇順に並び替え

            string o_readBunkatu = "-,-,-,-,-";

            // 文字コードを指定
            Encoding enc = Encoding.GetEncoding("Shift_JIS");

            // ファイルを開く
            StreamWriter writer = new StreamWriter(System.IO.Directory.GetCurrentDirectory() + "\\make_CSV.csv", false, enc);

            //ヘッダー設定
            writer.Write("前日のデータ,始値,高値,安値,終値");
            for(int i = 0;i<61;i++)
            {
                writer.Write(",日時,始値,高値,安値,終値");
            }
            writer.Write("\r\n");

            for (int i = 1; i < csvFile.Length; i++)//読み込みファイル分ループ
            {
                StreamReader read = new StreamReader(csvFile[i], Encoding.GetEncoding("Shift_JIS"));
                string allRead = read.ReadToEnd().Replace("\n", "@");
                string[] readBunkatu = allRead.Split('@');
                string kakikomi = "";
                string renketu;

                kakikomi += o_readBunkatu + ","; //前日最後の1分保存

                for (int j = 0;j < 61;j++)//60分間保存
                {
                    renketu = readBunkatu[1 + j].Insert(4, "/").Insert(7, "/").Insert(10," ").Insert(13,":");
                    kakikomi += renketu + ",";
                }

                renketu = readBunkatu[readBunkatu.Length - 2].Insert(4, "/").Insert(7, "/").Insert(10, " ").Insert(13, ":");
                o_readBunkatu = renketu; //前日最後の1分保存

                // パターン情報を記載
                writer.WriteLine(kakikomi);
                read.Close();

            }
            // ファイルを閉じる
            writer.Close();
            MessageBox.Show("書き込み完了");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DDBOXFOLDER.Text = "";
        }

        private void DDBOXFOLDER_TextChanged(object sender, EventArgs e)
        {

        }
    }


}
