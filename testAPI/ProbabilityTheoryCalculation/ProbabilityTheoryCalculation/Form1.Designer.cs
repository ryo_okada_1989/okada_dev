﻿namespace ProbabilityTheoryCalculation
{
    partial class Form1
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.startBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.moving = new System.Windows.Forms.Label();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.startMonth = new System.Windows.Forms.NumericUpDown();
            this.startDay = new System.Windows.Forms.NumericUpDown();
            this.endMonth = new System.Windows.Forms.NumericUpDown();
            this.endDay = new System.Windows.Forms.NumericUpDown();
            this.startYear = new System.Windows.Forms.NumericUpDown();
            this.endYear = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.startMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.startDay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.endMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.endDay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.startYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.endYear)).BeginInit();
            this.SuspendLayout();
            // 
            // startBtn
            // 
            this.startBtn.Font = new System.Drawing.Font("MS UI Gothic", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.startBtn.Location = new System.Drawing.Point(9, 177);
            this.startBtn.Name = "startBtn";
            this.startBtn.Size = new System.Drawing.Size(331, 65);
            this.startBtn.TabIndex = 6;
            this.startBtn.Text = "計算開始";
            this.startBtn.UseVisualStyleBackColor = true;
            this.startBtn.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS UI Gothic", 10F);
            this.label1.Location = new System.Drawing.Point(38, 106);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(266, 28);
            this.label1.TabIndex = 1;
            this.label1.Text = "※今日の日付は超えないように設定してください\r\n　 超えていた場合は現在時刻で計算します";
            // 
            // moving
            // 
            this.moving.AutoSize = true;
            this.moving.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.moving.Location = new System.Drawing.Point(149, 255);
            this.moving.Name = "moving";
            this.moving.Size = new System.Drawing.Size(56, 16);
            this.moving.TabIndex = 3;
            this.moving.Text = "計算中";
            // 
            // timer2
            // 
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS UI Gothic", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(12, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 19);
            this.label2.TabIndex = 1;
            this.label2.Text = "開始日付";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS UI Gothic", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(12, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 19);
            this.label3.TabIndex = 1;
            this.label3.Text = "終了日付";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("MS UI Gothic", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(169, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(28, 19);
            this.label4.TabIndex = 1;
            this.label4.Text = "年";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("MS UI Gothic", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.Location = new System.Drawing.Point(242, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(28, 19);
            this.label5.TabIndex = 1;
            this.label5.Text = "月";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("MS UI Gothic", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label6.Location = new System.Drawing.Point(319, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(28, 19);
            this.label6.TabIndex = 1;
            this.label6.Text = "日";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("MS UI Gothic", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label7.Location = new System.Drawing.Point(169, 69);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(28, 19);
            this.label7.TabIndex = 1;
            this.label7.Text = "年";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("MS UI Gothic", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label8.Location = new System.Drawing.Point(242, 69);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(28, 19);
            this.label8.TabIndex = 1;
            this.label8.Text = "月";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("MS UI Gothic", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label9.Location = new System.Drawing.Point(319, 69);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(28, 19);
            this.label9.TabIndex = 1;
            this.label9.Text = "日";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("MS UI Gothic", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label10.Location = new System.Drawing.Point(37, 41);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(28, 19);
            this.label10.TabIndex = 1;
            this.label10.Text = "～";
            // 
            // startMonth
            // 
            this.startMonth.Font = new System.Drawing.Font("MS UI Gothic", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.startMonth.Location = new System.Drawing.Point(203, 7);
            this.startMonth.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.startMonth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.startMonth.Name = "startMonth";
            this.startMonth.Size = new System.Drawing.Size(39, 26);
            this.startMonth.TabIndex = 7;
            this.startMonth.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // startDay
            // 
            this.startDay.Font = new System.Drawing.Font("MS UI Gothic", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.startDay.Location = new System.Drawing.Point(274, 7);
            this.startDay.Maximum = new decimal(new int[] {
            31,
            0,
            0,
            0});
            this.startDay.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.startDay.Name = "startDay";
            this.startDay.Size = new System.Drawing.Size(39, 26);
            this.startDay.TabIndex = 7;
            this.startDay.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // endMonth
            // 
            this.endMonth.Font = new System.Drawing.Font("MS UI Gothic", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.endMonth.Location = new System.Drawing.Point(203, 67);
            this.endMonth.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.endMonth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.endMonth.Name = "endMonth";
            this.endMonth.Size = new System.Drawing.Size(39, 26);
            this.endMonth.TabIndex = 7;
            this.endMonth.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // endDay
            // 
            this.endDay.Font = new System.Drawing.Font("MS UI Gothic", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.endDay.Location = new System.Drawing.Point(274, 67);
            this.endDay.Maximum = new decimal(new int[] {
            31,
            0,
            0,
            0});
            this.endDay.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.endDay.Name = "endDay";
            this.endDay.Size = new System.Drawing.Size(39, 26);
            this.endDay.TabIndex = 7;
            this.endDay.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // startYear
            // 
            this.startYear.Font = new System.Drawing.Font("MS UI Gothic", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.startYear.Location = new System.Drawing.Point(103, 7);
            this.startYear.Maximum = new decimal(new int[] {
            2500,
            0,
            0,
            0});
            this.startYear.Minimum = new decimal(new int[] {
            1850,
            0,
            0,
            0});
            this.startYear.Name = "startYear";
            this.startYear.Size = new System.Drawing.Size(65, 26);
            this.startYear.TabIndex = 7;
            this.startYear.Value = new decimal(new int[] {
            2020,
            0,
            0,
            0});
            // 
            // endYear
            // 
            this.endYear.Font = new System.Drawing.Font("MS UI Gothic", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.endYear.Location = new System.Drawing.Point(103, 67);
            this.endYear.Maximum = new decimal(new int[] {
            2500,
            0,
            0,
            0});
            this.endYear.Minimum = new decimal(new int[] {
            1850,
            0,
            0,
            0});
            this.endYear.Name = "endYear";
            this.endYear.Size = new System.Drawing.Size(65, 26);
            this.endYear.TabIndex = 7;
            this.endYear.Value = new decimal(new int[] {
            2020,
            0,
            0,
            0});
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(350, 290);
            this.Controls.Add(this.endDay);
            this.Controls.Add(this.endMonth);
            this.Controls.Add(this.startDay);
            this.Controls.Add(this.endYear);
            this.Controls.Add(this.startYear);
            this.Controls.Add(this.startMonth);
            this.Controls.Add(this.moving);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.startBtn);
            this.Name = "Form1";
            this.Text = "不変の法則シミュレータ";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.startMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.startDay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.endMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.endDay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.startYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.endYear)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button startBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label moving;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown startMonth;
        private System.Windows.Forms.NumericUpDown startDay;
        private System.Windows.Forms.NumericUpDown endMonth;
        private System.Windows.Forms.NumericUpDown endDay;
        private System.Windows.Forms.NumericUpDown startYear;
        private System.Windows.Forms.NumericUpDown endYear;
    }
}

