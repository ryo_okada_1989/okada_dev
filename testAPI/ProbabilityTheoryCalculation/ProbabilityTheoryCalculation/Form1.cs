﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;

namespace ProbabilityTheoryCalculation
{
    public partial class Form1 : Form
    {
        #region■変数

        /// <summary>
        /// エントリー実行ロジックタスク
        /// </summary>
        private BackgroundWorker bWorkerMain = null;

        /// <summary>
        /// 点滅用
        /// </summary>
        private int time_count = 0;
        #endregion

        private void Init()
        {
            //スレッド処理作成
            this.bWorkerMain = new BackgroundWorker()
            {
                WorkerSupportsCancellation = true, //キャンセルできるように
            };

            this.bWorkerMain.DoWork += (object sender2, DoWorkEventArgs e2) =>
            {
                e2.Result = DoWork(sender2, e2);
            };

            this.bWorkerMain.RunWorkerCompleted += (object sender3, RunWorkerCompletedEventArgs e3) =>
            {
                RunWorkerCompSaya(e3);
            };
        }


        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            timer2.Interval = 100;
            timer2.Enabled = false;
            moving.Visible = false;
        }


        /// <summary>
        /// バックグランドワーカー処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        private string DoWork(object sender, DoWorkEventArgs e)
        {
            DateTime start;             //検証開始日
            DateTime end;               //検証終了日
            DateTime move;              //時間経過表示用
            DateTime now = DateTime.Now;

            int judge = 0;      // 偶数、奇数判断
            int ashi = 24 * 60; // 1分足の１日の数
            int amount = 1;     // 出来高
            int roop_time = 60; // データ作成回数

            decimal line = 0;           //チャート表示用数値
            decimal move_value = 0.01M;   //変動値
            decimal start_value = 105;  //始値
            decimal end_value = start_value;//終値
            decimal hight_value;        //高値
            decimal low_value;          //安値

            string t_move;      // テンポラリ時間経過
            
            double test_count = 0; //検証回数

            Random r1 = new System.Random();    //ランダム数値

            //検証回数計算
            try
            {
                start = new System.DateTime(int.Parse(startYear.Text), int.Parse(startMonth.Text), int.Parse(startDay.Text));   //検証開始日
                end = new System.DateTime(int.Parse(endYear.Text), int.Parse(endMonth.Text), int.Parse(endDay.Text));           //検証終了日
            }
            catch
            {
                return "day_NG";
            }

            move = start;  //時間経過表示用

            if (end >= now) end = now;          //現在に変更
            if (start >= end) return "out";     //日付設定ミス

            // 文字コードを指定
            Encoding enc = Encoding.GetEncoding("Shift_JIS");

            // ファイルを開く
            StreamWriter writer = new StreamWriter(System.IO.Directory.GetCurrentDirectory() + "\\AUDCAD1.csv", false, enc);

            test_count = (end - start).TotalDays;
            test_count = test_count * ashi;  //指定した期間の１分足の数



            //1~10000からランダムで数値を選んで偶数、奇数か判断
            for (double i = 1; i <= test_count; i++)
            {
                //出力時刻の計算
                move = move.AddMinutes(1); //１分加算
                t_move = move.ToString("yyyy.MM.dd,HH:mm");
                writer.Write(t_move.Replace(" ",",") + ",");

                //初期化
                start_value = end_value;
                line = start_value; //始値代入
                low_value = start_value;      //安値
                hight_value = 0;    //高値

                //60秒分回す
                for (int j = 0; j <= roop_time;j++)
                {
                    judge = r1.Next(0, 10000) % 2;

                    if (judge == 0)
                    {
                        line = line + move_value;
                    }
                    else if (judge >= 1)
                    {
                        line = line - move_value;
                    }

                    //高値、安値更新
                    if (line >= hight_value) hight_value = line;
                    if (line <= low_value) low_value = line;

                }

                end_value = line;


                //judge = r1.Next(0, 10000) % 2;

                //if(judge == 0)
                //{
                //    //偶数なら＋１
                //    line++;
                //}
                //else if(judge >= 1)
                //{
                //    //奇数ならー１
                //    line--;
                //}

                //出力
                writer.WriteLine(start_value + "," + hight_value + "," + low_value + "," + end_value + "," + amount);
            }
            writer.Close();

            return "";
        }

        /// <summary>
        /// バックグランドワーカー完了時処理
        /// </summary>
        /// <param name="e"></param>
        private void RunWorkerCompSaya(RunWorkerCompletedEventArgs e)
        {
            startBtn.Enabled = true;        //ボタン活性
            timer2.Enabled = false;         //タイマー無効化
            timer2.Dispose();               //タイマー削除
            moving.Visible = false;         //計算中非表示

            //タスク無効化
            if (this.bWorkerMain != null)
            {
                if (this.bWorkerMain.IsBusy) this.bWorkerMain.CancelAsync();
                this.bWorkerMain.Dispose();
                this.bWorkerMain = null;
            }

            if(e.Result.ToString() == "out")            MessageBox.Show("終了日は開始日より後の日付を設定してください");
            else if (e.Result.ToString() == "day_NG")   MessageBox.Show("日付を正しく入力してください");
            else                                        MessageBox.Show("計算完了");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Init();

            //ボタン非活性
            startBtn.Enabled = false;

            timer2.Enabled = true;

            //スレッド実行
            this.bWorkerMain.RunWorkerAsync();
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            //動作中は「計算中」と点滅表示
            if (time_count % 3 == 0) moving.Visible = true;
            else moving.Visible = false;

            time_count++;
        }
    }
}
